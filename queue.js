let collection = [];

// Function to print the queue
function print() {
    return collection;
}

// Function to add an element to the back of the queue
function enqueue(element) {
    //add code here
    collection[collection.length] = element;
    return collection;
}

// Function to remove and return the element at the front of the queue
function dequeue() {
    for (let i = 0; i < collection.length - 1; i++) {
        collection[i] = collection[i + 1];
    }
    // collection.length = collection.length - 1;
    collection.length--;
    return collection;
}
// Function to get the element at the front of the queue without removing it
function front() {
    if (!isEmpty()) {
        return collection[0];
    } else {
        return undefined; // Queue is empty, return undefined
    }
}

// Function to get the size of the queue
function size() {
    return collection.length;
}

// Function to check if the queue is empty
function isEmpty() {
    return collection.length === 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
